﻿namespace Samples.Repositories
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Newtonsoft.Json;
    
    public class FileSystemCountryRepository
    {
        private readonly string filename;

        public FileSystemCountryRepository(string filename)
        {
            this.filename = filename;
        }

        public Task DeleteCountryFromDatabase(int id)
        {
            return this.AugmentCollection(x =>
                {
                    if (x.ContainsKey(id))
                        x.Remove(id);
                });
            
        }

        public Task AddCountryToDatabase(int id, string name)
        {
            return this.AugmentCollection(x => x[id] = name);
        }

        // in another scenario you may fetch from a database.
        // this is just a simple example.
        public Task<List<KeyValuePair<int, string>>> GetCountriesFromDatabase()
        {
            return Task.Factory.StartNew(() =>
                {
                    if (!File.Exists(this.filename))
                    {
                        return new List<KeyValuePair<int, string>>();
                    }
                    
                    var savedItems = JsonConvert.DeserializeObject<Dictionary<int, string>>(File.ReadAllText(this.filename));
                    return savedItems.ToList();
                                             
                });
        }
        
        private Task AugmentCollection(Action<Dictionary<int, string>> processor)
        {
            if (processor == null)
            {
                return Task.FromResult(true);
            }
            
            return Task.Factory.StartNew(() =>
                {
                    Dictionary<int, string> localKnownCountries = new Dictionary<int, string>();
                    if (File.Exists(this.filename))
                    {
                        var content = File.ReadAllText(filename);
                        localKnownCountries = JsonConvert.DeserializeObject<Dictionary<int, string>>(content);
                    }
                                             
                    processor(localKnownCountries);
                                             
                    var outputContent = JsonConvert.SerializeObject(localKnownCountries);
                    File.WriteAllText(this.filename, outputContent);
                });
        }
    }
}
