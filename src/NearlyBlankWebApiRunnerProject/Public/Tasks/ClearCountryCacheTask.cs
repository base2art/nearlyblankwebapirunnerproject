﻿namespace Samples.Public.Tasks
{
    using System.Threading.Tasks;
    using Samples.Public.Endpoints;
    
    public class ClearCountryCacheTask
    {
        public Task ExecuteAsync()
        {
            CountryService.ClearCachedData();
            return Task.FromResult(true);
        }
    }
}
