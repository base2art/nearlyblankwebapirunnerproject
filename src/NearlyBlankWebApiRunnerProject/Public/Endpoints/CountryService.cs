﻿namespace Samples.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Samples.Repositories;

    public class CountryService
    {
        private readonly FileSystemCountryRepository countries = new FileSystemCountryRepository("countries.cache");
        
        private static Dictionary<int, string> knownCountries = null;
        
        public async Task<List<KeyValuePair<int, string>>> GetCountries()
        {
            var localKnownCountries = knownCountries;
            if (localKnownCountries == null)
            {
                Func<Task<List<KeyValuePair<int, string>>>> lookupData = async () =>{
                    try
                    {
                        var result = await this.countries.GetCountriesFromDatabase();
                        Healthchecks.CountryServiceHealthCheck.Clear();
                        return result;
                    }
                    catch (Exception ex)
                    {
                        Healthchecks.CountryServiceHealthCheck.AddException(ex);
                        throw;
                    }
                };
                
                
                // avoid null refs and locking...
                // but you might fetch the data 2 times...
                localKnownCountries = (await lookupData()).ToDictionary(x => x.Key, x => x.Value);
                knownCountries = localKnownCountries;
            }
            
            return localKnownCountries.ToList();
        }
        
        public async Task AddCountry(int id, string name)
        {
            await this.countries.AddCountryToDatabase(id, name);
            ClearCachedData();
        }

        public async Task DeleteCountry(int id)
        {
            await this.countries.DeleteCountryFromDatabase(id);
            ClearCachedData();
        }

        public static void ClearCachedData()
        {
            knownCountries = null;
        }
    }
}
