﻿namespace Samples.Public.Healthchecks
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    
    public class CountryServiceHealthCheck
    {
        private static readonly List<Exception> lastExceptions = new List<Exception>();
        
        public Task ExecuteAsync()
        {
            var items = lastExceptions.Skip(0).Take(4).ToArray();
            if (items.Length > 4)
            {
                throw new AggregateException(items);
            }
            
            return Task.FromResult(true);
        }
        
        public static void AddException(Exception ex)
        {
            lastExceptions.Insert(0, ex);
        }

        public static void Clear()
        {
            lastExceptions.Clear();
        }
    }
}
